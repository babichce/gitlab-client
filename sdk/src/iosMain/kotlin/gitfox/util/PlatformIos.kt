package gitfox.util

import kotlin.system.getTimeMillis


internal actual fun currentTimeMillis(): Long = getTimeMillis()